#!/bin/bash
yum install python3-devel python3-pip -y

pip3 install requests psutil netifaces
pip3 install -U requests
git clone https://netzarydeveloper@bitbucket.org/netzarydeveloper/ch_agents.git

useradd manager

echo "hB05ntCDb4rh"

# useradd asabhi && echo "QWRtaW43Mw==" | base64 -d | passwd --stdin asabhi

echo "hB05ntCDb4rh" | passwd --stdin manager

cp -rv ch_agents/agents /home/manager

cd /home/manager/agents

cat > local_settings.py <<EOF
SERVER ="https://helpdesk.netzary.com" # CHANGE THIS TO https://helpdesk.bangalore2.com

API_TOKEN ="c7bdfc6dbf746466cf2cb3a642a659596dd791a4c3600694d79e13e8db0c3afa" 
EOF

/usr/bin/python3 register_server.py

cat > /etc/systemd/system/vscan.service <<EOF
[Unit]
Description=This service will find all suspecious files.
After=multi-user.target

[Service]
Type=simple
ExecStart=/usr/bin/python3 /home/manager/agents/infopush.py

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl restart vscan.service
systemctl enable vscan.service
systemctl status vscan.service